function formatListData() {
  var obj = {
    "list": [
      {
        "gtype": 10,
        "goods_id": "45278071",
        "brand_id": "0",
        "shop_id": "2880403",
        "source_type": "0",
        "goods_type": 0,
        "goods_code": "10289029",
        "is_hot": 1,
        "good_rate": "3.5",
        "shipping": 0,
        "cprice": 35,
        "oprice": 99,
        "tuan_price": 19.9,
        "is_vip": 0,
        "only_phone": 0,
        "hot_type": 3,
        "pic_url": "//s4.juancdn.com/bao/170608/f/c/59392308ad0a492a0d8b457b_800x800.jpg?imageMogr2/thumbnail/310x310!/quality/88!/format/jpg",
        "title": "喇叭袖条纹T恤套装",
        "start_time": "1496973600",
        "end_time": "1497924000",
        "show_stime": "1496973600",
        "show_etime": "1497924000",
        "sale_mode": "3",
        "sales_type": "3",
        "status": 1,
        "sale_type": 0,
        "is_new": 0,
        "target_url": "//m.juanpi.com/brand/0?shop_id=2880403",
        "sort": 1,
        "target": "_blank",
        "residue": "剩18小时",
        "inventory": "900",
        "shop_logo": "//s4.juancdn.com/brand/170222/4/9/58ad389c151ad1e2278b45d5_180x90.png?imageMogr2/quality/88!/format/png",
        "coupon": {
          "fid": 107713,
          "ab_type": 8,
          "abName": "618新人开售下单礼",
          "abFullcutSum": 0,
          "rule_num": 1,
          "rules": [
            {
              "aeBankInfo": "满1返95",
              "aeExtendType": 3,
              "aeGidCouponId": 736,
              "aeGiftNum": 1,
              "aeGoodInfo": null,
              "aeId": 128946,
              "aeMinMoney": 1,
              "aeMoney": 95,
              "aeTypeInfo": null
            }
          ],
          "end_time": 1498010400,
          "start_time": 1497700800,
          "couponAbId": ""
        }
      },
      {
        "gtype": 10,
        "goods_id": "40768640",
        "brand_id": "1799450",
        "shop_id": "2201306",
        "source_type": "0",
        "goods_type": 0,
        "goods_code": "18199507",
        "is_hot": 1,
        "good_rate": "2.8",
        "shipping": 0,
        "cprice": 55,
        "oprice": 199,
        "tuan_price": 39.9,
        "is_vip": 0,
        "only_phone": 0,
        "hot_type": 3,
        "pic_url": "//s4.juancdn.com/bao/170601/0/2/592f9d77a43d1f12ee4a09a9_800x800.jpg?imageMogr2/thumbnail/310x310!/quality/88!/format/jpg",
        "title": "玲珑兰618大促专场",
        "start_time": "1497700800",
        "end_time": "1498305600",
        "show_stime": "1497700800",
        "show_etime": "1498305600",
        "sale_mode": "3",
        "sales_type": "3",
        "status": 1,
        "sale_type": 0,
        "is_new": 0,
        "target_url": "//m.juanpi.com/brand/1799450?shop_id=2201306",
        "sort": 2,
        "target": "_blank",
        "residue": "剩5天",
        "inventory": "40000",
        "shop_logo": "//s4.juancdn.com/brand/170228/5/1/58b51580151ad127778b45c0_180x90.png?imageMogr2/quality/88!/format/png",
        "coupon": {
          "fid": 108598,
          "ab_type": 4,
          "abName": "玲珑兰618大促专场",
          "abFullcutSum": 1,
          "rule_num": 1,
          "rules": [
            {
              "aeBankInfo": "满88元减25元",
              "aeExtendType": null,
              "aeGidCouponId": null,
              "aeGiftNum": null,
              "aeGoodInfo": null,
              "aeId": 148055,
              "aeMinMoney": 88,
              "aeMoney": 25,
              "aeTypeInfo": null
            }
          ],
          "end_time": 1498010400,
          "start_time": 1497700800,
          "couponAbId": 106614
        }
      },
      {
        "gtype": 10,
        "goods_id": "49417947",
        "brand_id": "1244549",
        "shop_id": "2483387",
        "source_type": "0",
        "goods_type": 0,
        "goods_code": "16639140",
        "is_hot": 1,
        "good_rate": "2.5",
        "shipping": 0,
        "cprice": 188,
        "oprice": 748,
        "tuan_price": 168,
        "is_vip": 0,
        "only_phone": 0,
        "hot_type": 3,
        "pic_url": "//s4.juancdn.com/bao/170503/f/4/59098282a43d1f57eb08a266_800x800.jpg?imageMogr2/thumbnail/310x310!/quality/88!/format/jpg",
        "title": "希娅618大促专场",
        "start_time": "1497700800",
        "end_time": "1498305600",
        "show_stime": "1497700800",
        "show_etime": "1498305600",
        "sale_mode": "3",
        "sales_type": "3",
        "status": 1,
        "sale_type": 0,
        "is_new": 0,
        "target_url": "//m.juanpi.com/brand/1244549?shop_id=2483387",
        "sort": 3,
        "target": "_blank",
        "residue": "剩5天",
        "inventory": "400",
        "shop_logo": "//s4.juancdn.com/bao/170320/c/b/58cf84a6a43d1f6d912e4f76_180x90.png?imageMogr2/quality/88!/format/png",
        "coupon": {
          "fid": 108598,
          "ab_type": 4,
          "abName": "希娅618大促专场",
          "abFullcutSum": 1,
          "rule_num": 1,
          "rules": [
            {
              "aeBankInfo": "满88元减25元",
              "aeExtendType": null,
              "aeGidCouponId": null,
              "aeGiftNum": null,
              "aeGoodInfo": null,
              "aeId": 148055,
              "aeMinMoney": 88,
              "aeMoney": 25,
              "aeTypeInfo": null
            }
          ],
          "end_time": 1498010400,
          "start_time": 1497700800,
          "couponAbId": 106610
        }
      },
      {
        "gtype": 10,
        "goods_id": "41218230",
        "brand_id": "0",
        "shop_id": "2604399",
        "source_type": "0",
        "goods_type": 0,
        "goods_code": "11859068",
        "is_hot": 1,
        "good_rate": "3.5",
        "shipping": 0,
        "cprice": 55,
        "oprice": 159,
        "tuan_price": 39,
        "is_vip": 0,
        "only_phone": 0,
        "hot_type": 3,
        "pic_url": "//s4.juancdn.com/bao/170521/d/3/59216d9ba43d1f349e5cde7c_800x800.jpg?imageMogr2/thumbnail/310x310!/quality/88!/format/jpg",
        "title": "裕淘曼618专场",
        "start_time": "1497700800",
        "end_time": "1498305600",
        "show_stime": "1497700800",
        "show_etime": "1498305600",
        "sale_mode": "3",
        "sales_type": "3",
        "status": 1,
        "sale_type": 0,
        "is_new": 0,
        "target_url": "//m.juanpi.com/brand/0?shop_id=2604399",
        "sort": 4,
        "target": "_blank",
        "residue": "剩5天",
        "inventory": "601",
        "shop_logo": "//s4.juancdn.com/bao/170518/6/0/591d64f6a43d1f2e820d3fa4_180x90.png?imageMogr2/quality/88!/format/png",
        "coupon": {
          "fid": 108598,
          "ab_type": 4,
          "abName": "裕淘曼618专场",
          "abFullcutSum": 1,
          "rule_num": 1,
          "rules": [
            {
              "aeBankInfo": "满88元减25元",
              "aeExtendType": null,
              "aeGidCouponId": null,
              "aeGiftNum": null,
              "aeGoodInfo": null,
              "aeId": 148055,
              "aeMinMoney": 88,
              "aeMoney": 25,
              "aeTypeInfo": null
            }
          ],
          "end_time": 1498010400,
          "start_time": 1497700800,
          "couponAbId": 108484
        }
      }
    ],
    "page": 1,
    "pagesize": 10,
    "count": 2427,
    "dv": "6259477db07efc6",
    "cdn": 170,
    "more": 1,
    "code": 1000
  }

  return obj;
}

module.exports = {
  formatListData: formatListData
}