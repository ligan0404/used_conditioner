// pages/my/index.js
var app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    movies: [],
    info:{},
    taokouling:'',
    url1: app.d.hostImg+'dongben_car01.png',
    url2: app.d.hostImg + 'dongben_car02.png'
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    wx.showLoading({
      title: '加载中',
    })
    this.getDetail(options.id);
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
  
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
  
  },
  

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
  
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
  
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
  
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
  
  },

  /**
   * 用户点击右上角分--享
   */
  onShareAppMessage: function () {
    return {
      title: '批发价：' + this.data.info.money + '万元 ' + this.data.info.type + ' ' + this.data.info.license_time ,
      desc: '精英车商批发',
      path: '/pages/my/index?id='+this.data.info.id,
      imageUrl: this.data.info.main_image
    }
  },

  //获取详情
  getDetail:function(id){
    var that = this;
    wx.request({
      url: app.d.hostUrl + 'api/car/get_car',
      data: {
        id:id
      },
      method: 'POST',
      header: {
        'content-type': 'application/x-www-form-urlencoded',
        'mchnt': getApp().d.mchnt
      },
      success: function (res) {
        that.setData({
          info: res.data.data,
          movies: res.data.data.movies,
          taokouling: res.data.data.taokouling
        });
        wx.hideLoading();
      },
      fail: function () {
        // fail
        wx.showToast({
          title: '网络请求异常',
          duration: 2000
        });
      }
    });
  },

  //去我要发布
  gotoIndex:function(){
    wx.switchTab({
      url: '../index/index',
    })
  },

  calling: function (e) {
    console.log(e);
    var that = this;
    var id = e.currentTarget.dataset.id;
    var phone = e.currentTarget.dataset.phone;
    wx.makePhoneCall({
      phoneNumber: phone,
      success: function () {
        console.log("拨打电话成功")
        that.incrCall(id)
      }
    })
  },


  //复制
  copyTbl: function (e) {
    var that = this;
    console.log(that.data.taokouling)
    wx.setClipboardData({
      data: that.data.taokouling,
      success: function (res) { 
        wx.showToast({
          title: '已复制',
        })
      }
    });
  },


  incrCall: function (id) {
    wx.request({
      url: app.d.hostUrl + 'api/car/call',
      data: {
        id: id
      },
      method: 'POST',
      header: {
        'content-type': 'application/x-www-form-urlencoded' // 默认值
      },
      success: function (res) {
        console.log('拨打电话上传结果：=>' + res)
      },
      fail: function () {
        // fail
        wx.showToast({
          title: '网络请求异常',
          duration: 2000
        });
      }
    });
  },

  show:function(e){
    var url = e.currentTarget.dataset.url;
    console.log('URL:'+url);
    wx.previewImage({
      urls: [url]
    })
  },

  //图片点击事件
  imgYu: function (event) {
    var src = event.currentTarget.dataset.src;//获取data-src
    var imgList = event.currentTarget.dataset.list;//获取data-list
    //图片预览
    wx.previewImage({
      current: src, // 当前显示图片的http链接
      urls: imgList // 需要预览的图片http链接列表
    })
  }
})