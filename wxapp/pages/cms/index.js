// pages/posts/post.js
var app = getApp();
var p = 1;
var totalPage = 1;
var uid = 0;

var GetList = function (that, p, keyword) {
  wx.request({
    url: app.d.hostUrl + 'api/car/list',
    data: {
      pageSize: 5,
      page: p,
      keyword: keyword,
      uid: uid
    },
    method: 'POST',
    header: {
      'content-type': 'application/x-www-form-urlencoded',
      'mchnt': getApp().d.mchnt 
    },
    success: function (res) {
      console.log(res.data.list.length)
      that.setData({
        isEmpty: (res.data.list.length == 0) ? true : false
      });

      that.setData({
        list: res.data.list
      });

      totalPage = res.data.totalPage;
      p = res.data.currPage;
      wx.hideLoading();

      that.setData({
        isFa: res.data.isFa
      })

      console.log(that.data.isFa);

      console.log('总页数：' + totalPage + '当前页：' + p)

      if (totalPage > p) {
        that.setData({
          preActive: 'active',
          nextActive: 'active'
        })
      }

      if (p == 1) {
        that.setData({
          preActive: '',
          nextActive: (totalPage == 1) ? '' : 'active'
        })
      }

      if (p == totalPage && totalPage != 1) {
        that.setData({
          preActive: 'active',
          nextActive: ''
        })
      }

    },
    fail: function () {
      // fail
      wx.showToast({
        title: '网络请求异常',
        duration: 2000
      });
    }
  });
}

//全局参数

Page({

  /**
   * 页面的初始数据
   */
  data: {
    'list': [],
    'hidden': false,
    "isEmpty": false,
    list: [],
    preActive: '',
    nextActive: 'active',
    minMoney: '',
    maxMoney: '',
    keyword: '',
    isEmpty: false,
    isFa: false
  },

  onLoad: function () {
    var that = this;
    that._refreshData();
  },

  onShow: function () {
    var that = this;
    that._refreshData();
  },

  /**
   * 用户点击右上角分--享
   */
  onShareAppMessage: function () {
    console.log('share');
  },


  _refreshData: function () {
    var that = this;
    uid = wx.getStorageSync('uid');
    console.log('刷新了:uid' + uid)
    if (uid){
      wx.showLoading({
        title: '加载中',
      })
      GetList(that, 1, '');
    }
  },


  //下一页
  nextPage: function () {
    var that = this;

    if (p == totalPage) {
      wx.showToast({
        title: '没有下一页了',
      })
      return;
    }
    p++;
    wx.showLoading({
      title: '加载中',
    })
    GetList(that, p, that.data.keyword);
  },

  //上一页
  prePage: function () {
    var that = this;

    if (p == 1) {
      wx.showToast({
        title: '没有上一页了',
      })
      return;
    }
    p--;
    wx.showLoading({
      title: '加载中',
    })
    GetList(that, p, that.data.keyword);
  },

  //搜索
  search: function (e) {
    var that = this;
    GetList(that, p, that.data.keyword);
  },

  //关键字
  watchKeyword: function (event) {
    console.log(event.detail.value);
    var that = this;
    that.setData({
      keyword: event.detail.value
    });
  },

  //操作按钮
  showOp: function (e) {
    var that = this;
    var id = e.currentTarget.dataset.id;
    var name = e.currentTarget.dataset.name;
    var istatus = e.currentTarget.dataset.istatus;
    console.log(e);
    wx.showActionSheet({
      itemList: ['修改为' + name, '发布到朋友圈', '编辑'],
      success: function (res) {
        if (res.tapIndex == 0) {
          that.changeStatus(id, istatus);
        } else if (res.tapIndex == 1) {
          wx.navigateTo({
            url: '../goods/share?id=' + id,
          })
        } else {
          wx.navigateTo({
            url: '../index/detail?id=' + id,
          })
        }
      },
      fail: function (res) {
        console.log(res.errMsg)
      }
    })
  },

  del: function (e) {
    var id = e.currentTarget.dataset.id;
    var that = this;
    wx.showModal({
      title: '提示',
      content: '确定要删除吗？',
      success: function (res) {
        if (res.confirm) {
          wx.request({
            url: app.d.hostUrl + 'api/car/del',
            data: {
              id: id
            },
            method: 'POST',
            header: {
              'content-type': 'application/x-www-form-urlencoded',
              'mchnt': getApp().d.mchnt 
            },
            success: function (res) {
              wx.showToast({
                title: '删除成功',
                duration: 2000
              });
              that._refreshData();
              wx.hideLoading();
            },
            fail: function () {
              // fail
              wx.showToast({
                title: '网络请求异常',
                duration: 2000
              });
            }
          });
        }
      }
    })
  },

  //修改状态
  changeStatus: function (id, istatus) {
    var that = this;
    wx.showLoading({
      title: '加载中',
    })
    wx.request({
      url: app.d.hostUrl + 'api/car/change_status',
      data: {
        id: id,
        status: 1 - istatus
      },
      method: 'POST',
      header: {
        'content-type': 'application/x-www-form-urlencoded',
        'mchnt': getApp().d.mchnt 
      },
      success: function (res) {
        wx.showToast({
          title: '修改成功',
          duration: 2000
        });
        that._refreshData();
        wx.hideLoading();
      },
      fail: function () {
        // fail
        wx.showToast({
          title: '网络请求异常',
          duration: 2000
        });
      }
    });
  },

  //去首页
  gotoIndex: function () {
    wx.switchTab({
      url: '../index/index',
    })
  },

  tiao: function (e) {
    console.log(e)
    wx.navigateTo({
      url: '../my/index?id=' + e.currentTarget.dataset.id,
    })
  },

  /**
  * 用户点击右上角分享
  */
  onShareAppMessage: function (res) {
    var shareObj = {
      title: '精英车商批发',
      desc: '精英车商批发',
      path: '/pages/goods/index'
    };

    console.log(res);

    if (res.from === 'button') {
      var money = res.target.dataset.money;
      var t = res.target.dataset.type;
      var license_time = res.target.dataset.license_time;
      var id = res.target.dataset.id;
      var image = res.target.dataset.image;
      // 来自页面内转发按钮
      shareObj.title = '批发价：' + money + '万元 ' + t + ' ' + license_time;
      shareObj.path = '/pages/my/index?id=' + id;
      shareObj.imageUrl = image;
    }
    return shareObj;
  }
})