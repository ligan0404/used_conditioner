//获取应用实例
var util = require('../../utils/util.js'); 
var app = getApp();
var p=1;
var totalPage = 1;
var GetList = function (that, p,minMoney,maxMoney, keyword, startTime, endTime) {
  wx.request({
    url: app.d.hostUrl + 'api/car/list',
    data: {
      pageSize: 5,
      page: p,
      minMoney: minMoney,
      maxMoney: maxMoney,
      keyword: keyword,
      startTime:startTime,
      endTime:endTime
    },
    method: 'POST',
    header: {
      'content-type': 'application/x-www-form-urlencoded',
      'mchnt': getApp().d.mchnt 
    },
    success: function (res) {
      console.log(res.data.list.length)
      that.setData({
        isEmpty: (res.data.list.length == 0) ? true :false
      });
      var old_list = that.data.list;
      for (var i = 0; i < res.data.list.length; i++) {
        old_list.push(res.data.list[i]);
      }
      that.setData({
        list: old_list
      });
      totalPage = res.data.totalPage;
      p = res.data.currPage;
      wx.hideLoading();
      console.log('总页数：' + totalPage + '当前页：' + p)
    },
    fail: function () {
      // fail
      wx.showToast({
        title: '网络请求异常',
        duration: 2000
      });
    }
  });
}


Page({
  data: {
    currType: 0,
    //当前类型
    "types": {},
    typeTree: {},
    catIsEmpty: false,
    picker1Value: 0,
    picker1Range: ['北京', '上海', '广州', '深圳'],
    timeValue: '08:08',
    dateValue: '2018-01-01',
    dateValues: '2018-10-13',
    list:[],
    preActive:'',
    nextActive:'active',
    minMoney:'',
    maxMoney: '',
    keyword:'',
    isEmpty: false
  },

  onLoad:function(){
    var that = this;
    var time = util.formatDateTime(new Date());
    that.setData({
      dateValues: time,
      keyword:''
    });

    wx.showLoading({
      title: '加载中',
    })
    GetList(that, 1,'','','','','');
  },

  onShow:function(){
  //   var that = this;
  //   var time = util.formatDateTime(new Date());
  //   that.setData({
  //     minMoney: '',
  //     maxMoney: '',
  //     keyword: '',
  //     dateValues: time
  //   })
  //   wx.showLoading({
  //     title: '加载中',
  //   })
  //   GetList(that, 1, that.data.minMoney, that.data.maxMoney, that.data.keyword, that.data.dateValue, that.data.dateValues);
  },

  normalPickerBindchange: function (e) {
    this.setData({
      picker1Value: e.detail.value
    })
  },
  timePickerBindchange: function (e) {
    this.setData({
      timeValue: e.detail.value
    })
  },
  datePickerBindchange: function (e) {
    var that = this;
    that.setData({
      dateValue: e.detail.value
    })
  },
  datePickerBindchanges: function (e) {
    var that = this;
    that.setData({
      dateValues: e.detail.value
    })
  },
  calling: function (e) {
    console.log(e);
    var that = this;
    var id = e.currentTarget.dataset.id;
    var phone = e.currentTarget.dataset.phone;
    
    wx.makePhoneCall({
      phoneNumber: phone,
      success: function () {
        console.log("拨打电话成功");
        //记录拨打次数
        that.incrCall(id);
      },
      fail: function () {
      }
    })
  },
  tiao:function (e) {
    console.log(e)
    wx.navigateTo({
      url: '../my/index?id=' + e.currentTarget.dataset.id,
    })
  },
  
  //下一页
  nextPage:function(){
    var that = this;

    if (p == totalPage) {
      wx.showToast({
        title: '没有下一页了',
      })
      return;
    }
    p++;
    wx.showLoading({
      title: '加载中',
    })
    GetList(that, p, that.data.minMoney, that.data.maxMoney, that.data.keyword, that.data.dateValue, that.data.dateValues);
  },

  //上一页
  prePage: function () {
    var that = this;
    if(p==1){
      wx.showToast({
        title: '没有上一页了',
      })
      return;
    }
    p--;
    wx.showLoading({
      title: '加载中',
    })
    GetList(that, p, that.data.minMoney, that.data.maxMoney, that.data.keyword, that.data.dateValue, that.data.dateValues);
  },

  //搜索
  search:function(e){
    var that = this;
    wx.showLoading({
      title: '加载中',
    })
    p=1;
    that.setData({
      list:[]
    });
    GetList(that,1, that.data.minMoney, that.data.maxMoney, that.data.keyword, that.data.dateValue, that.data.dateValues);
  },


  //最小金额
  watchMinMoney: function (event){
    console.log(event.detail.value);
    var that = this;
    that.setData({
      minMoney: event.detail.value
    });
  },

  //最大金额
  watchMaxMoney: function (event) {
    console.log(event.detail.value);
    var that = this;
    that.setData({
      maxMoney: event.detail.value
    });
  },

  //关键字
  watchKeyword: function (event){
    console.log(event.detail.value);
    var that = this;
    that.setData({
      keyword: event.detail.value
    });
  },

  incrCall:function(id){
    wx.request({
      url: app.d.hostUrl + 'api/car/call',
      data: {
        id:id
      },
      method: 'POST',
      header: {
        'content-type': 'application/x-www-form-urlencoded',
        'mchnt': getApp().d.mchnt 
      },
      success: function (res) {
        console.log('拨打电话上传结果：=>'+res)
      },
      fail: function () {
        // fail
        wx.showToast({
          title: '网络请求异常',
          duration: 2000
        });
      }
    });
  },


  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    console.log('-onReachBottom-');
    var that = this;
    console.log(p+'-'+totalPage);
    if (p == totalPage || totalPage==1) {
      return;
    }
    p++;
    wx.showLoading({
      title: '加载中',
    })
    console.log(p);
    GetList(that, p, that.data.minMoney, that.data.maxMoney, that.data.keyword, that.data.dateValue, that.data.dateValues);

  },

  onPullDownRefresh:function(){
    var that = this;
    wx.showLoading({
      title: '加载中',
    })
    p = 1;
    that.setData({
      list: []
    });
    GetList(that, 1, that.data.minMoney, that.data.maxMoney, that.data.keyword, that.data.dateValue, that.data.dateValues);
    setTimeout(function(){
      wx.stopPullDownRefresh();
    },1000);
  },


  /**
  * 用户点击右上角分--享
  */
  onShareAppMessage: function (res) {
    var shareObj = {
      title: '精英车商批发',
      desc: '精英车商批发',
      path: '/pages/goods/index'
    };

    console.log(res);

    if (res.from === 'button') {
      var money = res.target.dataset.money;
      var t = res.target.dataset.type;
      var license_time = res.target.dataset.license_time;
      var id = res.target.dataset.id;
      var image = res.target.dataset.image;
      // 来自页面内转发按钮
      shareObj.title = '批发价：' + money + '万元 ' + t + ' ' + license_time;
      shareObj.path = '/pages/my/index?id=' + id;
      shareObj.imageUrl = image;
    }
    return shareObj;
  }
})